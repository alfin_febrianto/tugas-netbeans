/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tugas_kelompok;

/**
 *
 * @author Alfin Febrianto
 */
public class Mixer extends Dapur{
    private String nomor;
    
    public Mixer(){
        super();
        nomor = "45678";
    }
    public Mixer(String m, String d, String o){
        super(m,d);
        nomor = o;
    }
    
    public String getnomor(){
        return nomor;
    }
    
    public void setnomor(String o){
        nomor = o;
    }
    
    public void cetak(){
        System.out.println("----Mixer----");
        super.cetak();
        System.out.println("Nomor Seri Mixer : " + nomor);
    }
    
    public static void main(String[] args){
        Mixer one = new Mixer();
        one.setMerk("Maspion");
        one.setdaya("Listrik");
        one.setnomor("235765876");
        one.cetak();
    }
}
