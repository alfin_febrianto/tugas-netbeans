/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tugas_kelompok;

/**
 *
 * @author Alfin Febrianto
 */
public class Dapur extends Elektronik{
    private String daya;
    
    public Dapur(){
        super();
        daya = "Listrik";
    }
    
    public Dapur(String d){
        daya = d;
    }
    
    public Dapur(String m, String d){
        super(m);
        daya = d;
    }
    
    public String getdaya(){
        return daya;
    }
    
    public void setdaya(String d){
        daya = d;
    }
    public void cetak(){
        super.cetak();
        System.out.println("Sumber Daya : " + daya);
    }
    
    public static void main(String[] args){
        Dapur one = new Dapur();
        one.setMerk("Maspion");
        one.setdaya("Listrik");
        one.cetak();
    }
}
