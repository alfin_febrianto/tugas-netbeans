/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tugas_kelompok;

/**
 *
 * @author Alfin Febrianto
 */
public class Kulkas extends Dapur {
    private String garansi;
    private String color;
    
    public Kulkas(){
        super();
        garansi = "3 Tahun";
    }
    
    public Kulkas(String m,String d, String g){
        super(m,d);
        garansi = g;
    }
    
    public String getgaransi(){
        return garansi;
    }
    
    public void setgaransi(String g){
        garansi = g;
    }
    
    public String getcolor(){
        return color;
    }
    
    public void setcolor(String c){
        color = c;
    }
    
    public void cetak(){
        System.out.println("----Kulkas----");
        super.cetak();
        System.out.println("Garansi Kulkas : " + garansi);
        System.out.println("Color : " + color);
    }
    
    public static void main(String[] args){
        Kulkas one = new Kulkas();
        one.setMerk("Maspion");
        one.setdaya("Listrik");
        one.setgaransi("3 Tahun");
        one.setcolor("Merah");
        one.cetak();
    }
}
