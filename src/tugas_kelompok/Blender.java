/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tugas_kelompok;

/**
 *
 * @author Alfin Febrianto
 */
public class Blender extends Dapur{
    private String kegunaan;
    
    public Blender(){
        super();
        kegunaan = "Mengaduk Makanan";
    }
    
    public Blender(String m,String d, String k){
        super(m,d);
        kegunaan = k;
    }
    
    public String getkegunaan(){
        return kegunaan;
    }
    
    public void setkegunaan(String k){
        kegunaan = k;
    }
    
    public void cetak(){
        System.out.println("----Blender----");
        super.cetak();
        System.out.println("Kegunaan Blender : " + kegunaan);
    }
    
    public static void main(String[] args){
        Blender one = new Blender();
        one.setMerk("Maspion");
        one.setdaya("Listrik");
        one.setkegunaan("Mengaduk Makanan");
        one.cetak();
    }
}
