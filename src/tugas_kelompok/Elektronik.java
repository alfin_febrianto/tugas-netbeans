/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tugas_kelompok;

/**
 *
 * @author Alfin Febrianto
 */
public class Elektronik{
    private String merk;
    
    public void cetak(){
        System.out.println("Nama Merek : " + merk);
    }
    
    public Elektronik(){
        merk = "Maspion";
    }
    
    public Elektronik(String m){
        merk = m;
    }
    
    public String getMerk(){
        return merk;
    }
    
    public void setMerk(String m){
        merk = m;
    }
    
    public static void main(String[] args){
        Elektronik satu = new Elektronik();
        satu.setMerk("Maspion");
        satu.cetak();
    }
}
