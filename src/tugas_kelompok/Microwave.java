/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tugas_kelompok;

/**
 *
 * @author Alfin Febrianto
 */
public class Microwave extends Dapur{
    private String tahun;
    
    public Microwave(){
        super();
        tahun = "2015";
    }
    public Microwave(String m, String d,String t){
        super(m,d);
        tahun = t;
    }
    
    public String gettahun(){
        return tahun;
    }
    
    public void settahun(String t){
        tahun = t;
    }
    
    public void cetak(){
        System.out.println("----Microwave----");
        super.cetak();
        System.out.println("Tahun Pembuatan Microwave : " + tahun);
    }
    
    public static void main(String[] args){
        Microwave one = new Microwave();
        one.setMerk("Maspion");
        one.setdaya("Listrik");
        one.settahun("2016");
        one.cetak();
    }
}
